ARG ARCH=amd64
FROM balenalib/${ARCH}-alpine-node:11

COPY lib /opt/smartgram/lib
COPY entrypoint.sh /opt/smartgram
COPY package.json /opt/smartgram
COPY package-lock.json /opt/smartgram

WORKDIR /opt/smartgram

#crossRUN [ "cross-build-start" ]

RUN npm install --production \
    && npm cache clean --force \
    && rm -rf /tmp/*

#crossRUN [ "cross-build-end" ]

EXPOSE 3500

ENTRYPOINT ["/opt/smartgram/entrypoint.sh"]
CMD ["--port", "3500"]
