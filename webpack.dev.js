const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    mode: 'development',
    module: {
        rules: [
            {test: /\.scss/, loader: 'style-loader!css-loader!sass-loader'}
        ]
    },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './src',
        https: true,
        noInfo: false,
        hot: true,
        inline: true,
        host: '0.0.0.0',
        port: 3000,
        proxy: {
            '/signal': {
                target: 'ws://localhost:3500',
                pathRewrite: {'^/signal' : ''},
                ws: true
            },
        },
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        }
    }
});
