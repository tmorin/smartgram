const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
var SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const pkg = require('./package');

module.exports = {
    entry: {
        'index': path.resolve(__dirname, 'src/index.ts'),
        'designer': path.resolve(__dirname, 'src/designer.ts'),
        'player': path.resolve(__dirname, 'src/player.ts')
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: '[name].[hash].js',
        globalObject: `(typeof self !== 'undefined' ? self : this)`
    },
    module: {
        rules: [
            {test: /\.(ts|js)$/, exclude: /node_modules/, loader: 'babel-loader'},
            {test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=application/font-woff'},
            {test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=application/font-woff'},
            {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=application/octet-stream'},
            {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader'},
            {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=image/svg+xml'},
            {test: /\.(png|jpg)$/, loader: 'url-loader?limit=25000'}
        ]
    },
    resolve: {
        extensions: ['.ts', '.js', '.json']
    },
    plugins: [
        new CleanWebpackPlugin('public'),
        new CopyWebpackPlugin([
            {from: 'src', force: true, ignore: ['*.ts', '*.scss', '*.ejs']}
        ]),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'src/index.ejs'),
            filename: 'index.html',
            chunks: ['index'],
            pkg
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'src/designer.ejs'),
            filename: 'designer.html',
            chunks: ['designer'],
            pkg
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'src/player.ejs'),
            filename: 'player.html',
            chunks: ['player'],
            pkg
        }),
        new SWPrecacheWebpackPlugin({})
    ]
};

