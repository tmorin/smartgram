const fs = require('fs');
const {argv} = require('yargs');
const config = require('./conf');

if (argv.config) {
    Object.assign(
        config,
        JSON.parse(
            fs.readFileSync(argv.config).toString()
        )
    );
}

const io = require('socket.io')({
    cookie: false,
    cookiePath: false
});

io.origins(
    argv.origins ? argv.origins.split(',').map(o => o.trim()) : config.origins
);

io.on('connection', client => {
    const from = client.id;
    const roomId = client.handshake.query.roomId || from;
    if (roomId) {
        console.log('connection', client.id, 'joined', roomId);
        client.join(roomId, () => {
            client.in(roomId).emit('peer-joined', {from});

            client.in(roomId).on('send-offer', data => {
                console.log('send-offer', data);
                client.in(roomId).emit('offer-received', data);
            });

            client.in(roomId).on('send-answer', data => {
                console.log('send-answer', data);
                client.in(roomId).emit('answer-received', data);
            });

            client.in(roomId).on('send-candidate', data => {
                console.log('send-candidate', data);
                client.in(roomId).emit('candidate-received', data);
            });

            client.in(roomId).on('leave', data => {
                console.log('leave', data);
                client.in(roomId).emit('peer-left', data);
            });
        });
        client.on('disconnect', () => {
            client.in(roomId).emit('peer-left', {from});
        })
    } else {
        client.disconnect(true);
        console.log('connection', client.id, 'closed');
    }
});

const port = argv.port || config.port;
io.listen(port);
console.info('listening on %s', port);
console.info('origins', io.origins());
