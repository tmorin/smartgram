#!/usr/bin/env bash

VERSION="${1//\//_}"
VERSION="${VERSION:-latest}"

if [[ ${VERSION} =~ ^v[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    VERSION=$(echo ${VERSION} | sed -E "s/v([0-9]+\.[0-9]+\.[0-9]+)/\1/")
    VERSION_MAJOR=$(echo ${VERSION} | sed -E "s/([0-9]+)\.[0-9]+\.[0-9]+/\1/")
    VERSION_MINOR=$(echo ${VERSION} | sed -E "s/([0-9]+\.[0-9]+)\.[0-9]+/\1/")
    VERSIONS="${VERSION} ${VERSION_MAJOR} ${VERSION_MINOR} latest"
else
    VERSIONS="${VERSION}"
fi

BASE_IMAGE_SIGNAL="${2}/signal"

ARCHITECTURES="amd64 arm64 arm"

BASED_ARCH=$(docker system info -f "{{.Architecture}}")
if [[ ${BASED_ARCH} == "x86_64" ]]; then
    BASED_ARCH="amd64"
elif [[ ${BASED_ARCH} == "aarch64" ]]; then
    BASED_ARCH="arm64"
fi

echo VERISONS: ${VERSIONS}
echo BASE_IMAGE_SIGNAL: ${BASE_IMAGE_SIGNAL}
echo BASED_ARCH: ${BASED_ARCH}

# dockerfile architectures: amd64 armv7hf aarch64
# see https://www.balena.io/docs/reference/base-images/base-images/#-a-name-image-tree-a-balena-image-trees

# docker architecture: amd64(x86_64) arm arm64(aarch64)

function build() {
    local tags=""
    local arch_docker=$1
    local arch_dockerfile=${arch_docker}

    case ${arch_docker} in
        arm)
            arch_dockerfile=armv7hf
            ;;
        arm64)
            arch_dockerfile=aarch64
            ;;
    esac

    local dockerfile="Dockerfile"
    if [[ ${BASED_ARCH} != ${arch_docker} ]]; then
        echo "cross build for ${arch_docker}"
        mkdir -p .tmp
        sed -e 's/#cross//g' Dockerfile > .tmp/Dockerfile;
        dockerfile=".tmp/Dockerfile"
    fi

    local tag="${BASE_IMAGE_SIGNAL}/${arch_docker}:$VERSION"
    tags="${tags} ${tag}"
    echo "build $tag"
    docker build -f ${dockerfile} --build-arg ARCH=${arch_dockerfile} --no-cache -t ${tag} .
    if [[ $? -ne 0 ]]; then
        exit 1
    fi

    if [[ -n ${VERSION_MAJOR} ]]; then
        local tag_major="${BASE_IMAGE_SIGNAL}/${arch_docker}:$VERSION_MAJOR"
        tags="${tags} ${tag_major}"
        local tag_minor="${BASE_IMAGE_SIGNAL}/${arch_docker}:$VERSION_MINOR"
        tags="${tags} ${tag_minor}"
        local tag_latest="${BASE_IMAGE_SIGNAL}/${arch_docker}:latest"
        tags="${tags} ${tag_latest}"
        echo "tag $tag_major"
        echo "tag $tag_minor"
        echo "tag $tag_latest"
        docker tag ${tag} ${tag_major} \
            && docker tag ${tag} ${tag_minor} \
            && docker tag ${tag} ${tag_latest}
        if [[ $? -ne 0 ]]; then
            exit 2
        fi
    fi

    for tag in ${tags}; do
        echo "push ${tag}"
        docker push ${tag}
        if [[ $? -ne 0 ]]; then
            exit 3
        fi
    done
}

function manifest() {
    local version=$1
    local main_tag="${BASE_IMAGE_SIGNAL}:$VERSION"

    local tags=""
    for architecture in ${ARCHITECTURES}; do
        local tag="${BASE_IMAGE_SIGNAL}/${architecture}:$version"
        tags="${tags} ${tag}"
    done

    echo "build manifest for ${main_tag} ${tags}"

    docker --config .docker manifest create --amend ${main_tag} ${tags}

    for architecture in ${ARCHITECTURES}; do
        local tag="${BASE_IMAGE_SIGNAL}/${architecture}:$version"
        echo "annotate ${tag} with ${architecture}"
        docker --config .docker manifest annotate ${main_tag} ${tag} --arch ${architecture} --os linux
    done

    docker --config .docker manifest push -p ${main_tag}
    if [[ $? -ne 0 ]]; then
        exit 4
    fi
}

for architecture in ${ARCHITECTURES}; do
    build ${architecture}
    if [[ $? -ne 0 ]]; then
        exit 5
    fi
done

for version in ${VERSIONS}; do
    manifest ${version}
    if [[ $? -ne 0 ]]; then
        exit 6
    fi
done
