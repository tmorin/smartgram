const template = require('lodash.template');
const request = require('request-promise-native');

const args = require('yargs')
    .option('signal_origins', {
        string: true,
        default: '*:*'
    })
    .option('signal_tag', {
        string: true,
        default: 'latest'
    })
    .option('signal_host', {
        string: true,
        required: true
    })
    .option('traefik_network', {
        string: true,
        default: 'traefik_public'
    })
    .option('portainer_url', {
        required: true,
        string: true
    })
    .option('portainer_username', {
        string: true
    })
    .option('portainer_password', {
        string: true
    })
    .option('endpoint_id', {
        required: true,
        number: true,
        default: 1
    })
    .option('swarm_id', {
        required: true,
        string: true
    })
    .argv;

const signal_tag = args.signal_tag
    .replace(/\//g, '_')
    .replace(/v([0-9]*\.[0-9]*\.[0-9]*)/g, '$1');

const stackContent = template(`
version: "3.7"

services:
  signal:
    image: registry.gitlab.com/tmorin/smartgram/signal:<%= signal_tag %>
    networks:
      - <%= traefik_network %>
    command: --config /opt/smartgram/config.json
    deploy:
      labels:
        - traefik.frontend.rule=Host:<%= signal_host %>
        - traefik.docker.network=<%= traefik_network %>
        - traefik.port=3500
      mode: replicated
      replicas: 1
    configs:
      - source: smartgram-signal-config
        target: /opt/smartgram/config.json

configs:
  smartgram-signal-config:
    external: true

networks:
  <%= traefik_network %>:
    external: true`)(Object.assign({}, args, {signal_tag}));
console.log(stackContent);

const options = {
    json: true,
    headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
    },
    qs: {
        type: 1,
        method: 'string',
        endpointId: args.endpoint_id
    }
};

async function authenticate() {
    console.log('authenticate');
    try {
        const auth = await request(Object.assign({}, options, {
            uri: `${args.portainer_url}/api/auth`,
            method: 'POST',
            body: {
                username: args.portainer_username || process.env['PORTAINER_USERNAME'],
                password: args.portainer_password || process.env['PORTAINER_PASSWORD']
            }
        }));
        if (auth.jw) {
            console.error('no jwt returned');
            process.exit(1);
        }
        return auth.jwt
    } catch (e) {
        console.error('unable to authenticate');
        console.error(e.error);
        process.exit(1);
    }
}

async function getCurrentStackId(bearer, name) {
    console.log('getCurrentStackId', name);
    try {
        const stacks = await request(Object.assign({}, options, {
            uri: `${args.portainer_url}/api/stacks`,
            method: 'GET',
            auth: {bearer}
        }));
        return stacks.filter(s => s.Name === name)[0];
    } catch (e) {
        console.error('unable to get stack');
        console.error(e.error);
        process.exit(2);
    }
}

async function deployStack(bearer, name, content) {
    console.log('deployStack', name);
    try {
        await request(Object.assign({}, options, {
            uri: `${args.portainer_url}/api/stacks`,
            method: 'POST',
            auth: {bearer},
            body: {
                Name: name,
                SwarmId: args.swarm_id,
                StackFileContent: content
            }
        }));
    } catch (e) {
        console.error('unable to deploy stack');
        console.error(e.error);
        process.exit(3);
    }
}

async function updateStack(bearer, id, content) {
    console.log('updateStack', id);
    try {
        await request(Object.assign({}, options, {
            uri: `${args.portainer_url}/api/stacks/${id}`,
            method: 'PUT',
            auth: {bearer},
            body: {
                StackFileContent: content,
                prune: true
            }
        }));
    } catch (e) {
        console.error('unable to update stack');
        console.error(e.error);
        process.exit(4);
    }
}

(async function () {
    const name = 'smartgram';
    const bearer = await authenticate();
    const stack = await getCurrentStackId(bearer, name);
    if (stack) {
        await updateStack(bearer, stack.Id, stackContent);
    } else {
        await deployStack(bearer, name, stackContent);
    }
    process.exit(0);
}());
