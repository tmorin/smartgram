const webpackDev = require('./webpack.dev');

module.exports = (config) => {
    config.set({
        frameworks: ['mocha'],

        reporters: ['progress', 'junit'],

        files: [
            {pattern: 'test/**/*.spec.ts', watched: false}
        ],

        preprocessors: {
            'test/**/*.spec.ts': ['webpack']
        },

        webpack: {
            module: webpackDev.module,
            resolve: webpackDev.resolve,
            mode: webpackDev.mode,
            devtool: webpackDev.devtool
        },

        webpackMiddleware: {
            stats: 'errors-only'
        },

        junitReporter: {
            outputDir: '.junit'
        },

        client: {
            mocha: {
                reporter: 'html'
            }
        }
    })
};