import {expect} from 'chai';
import {Box} from '../src/box';
import {Point} from '../src/point';
import {Distance} from '../src/distance';

describe('box', function () {

    it('should set width and height', function () {
        const b1 = new Box(
            new Point(1, 2)
        );
        expect(b1.width.value).to.be.eq(1);
        expect(b1.height.value).to.be.eq(2);
        const b2 = b1.resize(new Distance(2), new Distance(3));
        expect(b2.width.value).to.be.eq(2);
        expect(b2.height.value).to.be.eq(3);
    });

    it('should scale', function () {
        const b1 = new Box(
            new Point(1, 2)
        );
        expect(b1.width.value).to.be.eq(1);
        expect(b1.height.value).to.be.eq(2);
        const b2 = b1.scaleTo(10);
        expect(b2.width.value).to.be.eq(10);
        expect(b2.height.value).to.be.eq(20);
    });

});