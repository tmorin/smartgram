import {Point} from '../src/point';
import {expect} from 'chai';

describe('point', function () {

    it('should translate', function () {
        const p = new Point(0, 0);
        const newP = p.translateX(1).translateY(2);
        expect(newP.x).to.be.eq(1);
        expect(newP.y).to.be.eq(2);
    });

    it('should compute distance', function () {
        const p1 = new Point(0, 0);
        const p2 = new Point(1, 0);
        const d = p1.distanceTo(p2);
        expect(d.value).to.be.eq(1);
    });

    it('should scale', function () {
        const p1 = new Point(1, 2);
        const newP = p1.scaleTo(10);
        expect(newP.x).to.be.eq(10);
        expect(newP.y).to.be.eq(20);
    });

});