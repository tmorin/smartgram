import {expect} from 'chai';
import {Distance} from '../src/distance';

describe('distance', function () {

    it('should add', function () {
        const d1 = new Distance(100);
        const d2 = new Distance(50);
        expect(d1.plus(d2).value).to.be.eq(150);
    });

    it('should remove', function () {
        const d1 = new Distance(100);
        const d2 = new Distance(50);
        expect(d1.minus(d2).value).to.be.eq(50);
    });

});