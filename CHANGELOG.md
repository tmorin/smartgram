# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.10.1"></a>
## [0.10.1](https://gitlab.com/tmorin/smartgram/compare/v0.10.0...v0.10.1) (2018-12-24)


### Bug Fixes

* the pattern can be cut when printed ([dc0eb39](https://gitlab.com/tmorin/smartgram/commit/dc0eb39))



<a name="0.10.0"></a>
# [0.10.0](https://gitlab.com/tmorin/smartgram/compare/v0.9.0...v0.10.0) (2018-12-19)


### Features

* **progressive:** provide only one manifest ([e729257](https://gitlab.com/tmorin/smartgram/commit/e729257))



<a name="0.9.0"></a>
# [0.9.0](https://gitlab.com/tmorin/smartgram/compare/v0.8.0...v0.9.0) (2018-12-18)


### Features

* **progressive:** transform to progressive app ([8bd3fd2](https://gitlab.com/tmorin/smartgram/commit/8bd3fd2))



<a name="0.8.0"></a>
# [0.8.0](https://gitlab.com/tmorin/smartgram/compare/v0.7.1...v0.8.0) (2018-12-17)


### Features

* **remote:** publish stream to remote devices ([00b21b7](https://gitlab.com/tmorin/smartgram/commit/00b21b7))



<a name="0.7.1"></a>
## [0.7.1](https://gitlab.com/tmorin/smartgram/compare/v0.7.0...v0.7.1) (2018-12-06)


### Bug Fixes

* designer UI is broken ([0323ced](https://gitlab.com/tmorin/smartgram/commit/0323ced))



<a name="0.7.0"></a>
# [0.7.0](https://gitlab.com/tmorin/smartgram/compare/v0.6.1...v0.7.0) (2018-12-04)


### Features

* **video:** play an uploaded video and crop stream [#12](https://gitlab.com/tmorin/smartgram/issues/12) ([ea37f39](https://gitlab.com/tmorin/smartgram/commit/ea37f39))



<a name="0.6.1"></a>
## [0.6.1](https://gitlab.com/tmorin/smartgram/compare/v0.6.0...v0.6.1) (2018-12-03)


### Bug Fixes

* **video:** videos were not loaded and played in chrome and mobile devices ([b2941d1](https://gitlab.com/tmorin/smartgram/commit/b2941d1))



<a name="0.6.0"></a>
# [0.6.0](https://gitlab.com/tmorin/smartgram/compare/v0.5.0...v0.6.0) (2018-12-03)


### Bug Fixes

* **video:** force height (50vh) of player when layout sm ([0f6e8bb](https://gitlab.com/tmorin/smartgram/commit/0f6e8bb))
* **video:** when fullscreen stream still keep the parent size ([5c92f1f](https://gitlab.com/tmorin/smartgram/commit/5c92f1f))


### Features

* **video:** add more samples ([fbcbea6](https://gitlab.com/tmorin/smartgram/commit/fbcbea6))



<a name="0.5.0"></a>
# [0.5.0](https://gitlab.com/tmorin/smartgram/compare/v0.4.0...v0.5.0) (2018-11-30)


### Bug Fixes

* **print:** add version from package.json ([890ce65](https://gitlab.com/tmorin/smartgram/commit/890ce65))


### Features

* **video:** generate simple video stream [#10](https://gitlab.com/tmorin/smartgram/issues/10) ([1d07ef1](https://gitlab.com/tmorin/smartgram/commit/1d07ef1))



<a name="0.4.0"></a>
# [0.4.0](https://gitlab.com/tmorin/smartgram/compare/v0.3.0...v0.4.0) (2018-11-29)


### Bug Fixes

* **print:** align the form and the overview ([fadd14a](https://gitlab.com/tmorin/smartgram/commit/fadd14a))


### Features

* **print:** compute patterns taking into account angle between ground and pyramid's faces [#11](https://gitlab.com/tmorin/smartgram/issues/11) ([af911c8](https://gitlab.com/tmorin/smartgram/commit/af911c8))



<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/tmorin/smartgram/compare/v0.2.0...v0.3.0) (2018-11-28)


### Features

* **app:** add favicon [#9](https://gitlab.com/tmorin/smartgram/issues/9) ([5e9416d](https://gitlab.com/tmorin/smartgram/commit/5e9416d))
* **print:** download and print the template [#7](https://gitlab.com/tmorin/smartgram/issues/7) ([4999f71](https://gitlab.com/tmorin/smartgram/commit/4999f71))



<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/tmorin/smartgram/compare/v0.1.0...v0.2.0) (2018-11-28)


### Bug Fixes

* **print:** prism properties are not hidden when printing [#6](https://gitlab.com/tmorin/smartgram/issues/6) ([f6e8e0f](https://gitlab.com/tmorin/smartgram/commit/f6e8e0f))


### Features

* **print:** add footer to page [#5](https://gitlab.com/tmorin/smartgram/issues/5) ([b5453c9](https://gitlab.com/tmorin/smartgram/commit/b5453c9))



<a name="0.1.0"></a>
# 0.1.0 (2018-11-28)


### Features

* **print:** calculate geometry of faces' pyramid from screen [#2](https://gitlab.com/tmorin/smartgram/issues/2) ([5cc988a](https://gitlab.com/tmorin/smartgram/commit/5cc988a))
