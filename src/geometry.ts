import {Distance} from './distance';
import {Angle} from './angle';
import {Point} from './point';
import {AngleUnit} from './units';

export interface Polygon {
    a: Point
    b: Point
    c: Point
    d: Point
    aB: Angle
    aC: Angle
    aG: Angle
}

export function scalePolygon(polygon: Polygon, factor: number): Polygon {
    return {
        a: polygon.a.scaleTo(factor),
        b: polygon.b.scaleTo(factor),
        c: polygon.c.scaleTo(factor),
        d: polygon.d.scaleTo(factor),
        aB: polygon.aB,
        aC: polygon.aC,
        aG: polygon.aG
    }
}

export function calculatePolygon(
    screenWidth: Distance,
    squareSize: Distance,
    aG: Angle
): Polygon {

    const dAE = screenWidth.minus(squareSize).divide(2).value;
    const aC = new Angle(90 - aG.valueAsDegree, AngleUnit.degree);
    const height = dAE / Math.tan(aC.valueAsRadiant);
    const aB = new Angle(90 - aC.valueAsDegree);

    const a = new Point(0, 0);
    const b = a.translateX(screenWidth.value);
    const c = b.translateX(-dAE).translateY(height);
    const d = c.translateX(-squareSize.value);

    return {a, b, c, d, aB, aC, aG};
}
