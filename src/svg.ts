import {Polygon} from './geometry';

export function createSvg(): SVGSVGElement {
    const svg = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'svg'
    ) as SVGSVGElement;

    svg.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

    return svg;
}

export function createPolygon(
    {a, b, c, d}: Polygon, id?: string
): SVGPolygonElement {
    const points = [a, b, c, d].map(p => `${p.x},${p.y}`).join(' ');

    const polygon = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'polygon'
    ) as SVGPolygonElement;

    polygon.setAttribute('points', points);
    polygon.setAttribute('fill', 'white');
    polygon.setAttribute('stroke', 'black');
    polygon.setAttribute('stroke-dasharray', '12.5');
    polygon.setAttribute('stroke-width', '4');

    if (id) {
        polygon.id = id;
    }

    return polygon;
}

export function usePolygon(
    ref: string
) {
    const use = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'use'
    ) as SVGUseElement;

    use.setAttribute('href', `#${ref}`);
    use.setAttribute('xlink:href', `#${ref}`);

    return use;
}

export function createLegends(
    {a, b, c, d, aG}: Polygon, scale: number, unit: string
): SVGGElement {
    const g = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'g'
    ) as SVGGElement;

    const text_ab = g.appendChild(
        document.createElementNS(
            'http://www.w3.org/2000/svg',
            'text'
        )
    );
    text_ab.textContent = `${Math.floor(a.distanceTo(b).value) / scale} ${unit}`;
    text_ab.style.fontSize = '1cm';
    text_ab.style.dominantBaseline = 'text-after-edge';
    text_ab.style.textAnchor = 'middle';
    text_ab.setAttribute('x', `${b.x / 2}`);
    text_ab.setAttribute('y', `${a.y}`);

    const text_dc = g.appendChild(
        document.createElementNS('http://www.w3.org/2000/svg', 'text')
    );
    text_dc.textContent = `${Math.floor(d.distanceTo(c).value) / scale} ${unit}`;
    text_dc.style.fontSize = '1cm';
    text_dc.style.dominantBaseline = 'text-before-edge';
    text_dc.style.textAnchor = 'middle';
    text_dc.setAttribute('x', `${b.x / 2}`);
    text_dc.setAttribute('y', `${d.y}`);

    const line_height = g.appendChild(
        document.createElementNS('http://www.w3.org/2000/svg', 'line')
    );
    line_height.setAttribute('stroke', 'black');
    line_height.setAttribute('stroke-dasharray', '12.5');
    line_height.setAttribute('stroke-width', '4');
    const line_p1 = a.translateX(a.distanceTo(b).value / 2);
    line_height.setAttribute('x1', `${line_p1.x}`);
    line_height.setAttribute('y1', `${line_p1.y}`);
    const line_p2 = d.translateX(d.distanceTo(c).value / 2);
    line_height.setAttribute('x2', `${line_p2.x}`);
    line_height.setAttribute('y2', `${line_p2.y}`);

    const text_height = g.appendChild(
        document.createElementNS('http://www.w3.org/2000/svg', 'text')
    );
    text_height.textContent = `${Math.floor(line_p1.distanceTo(line_p2).value) / scale} ${unit}`;
    text_height.style.fontSize = '1cm';
    text_height.style.dominantBaseline = 'text-after-edge';
    text_height.style.textAnchor = 'middle';
    const text_height_p = line_p1.translateY(line_p1.distanceTo(line_p2).value / 2);
    text_height.setAttribute('transform', `translate(${text_height_p.x}, ${text_height_p.y}) rotate(-90)`);

    const text_angle = g.appendChild(
        document.createElementNS('http://www.w3.org/2000/svg', 'text')
    );
    text_angle.textContent = `${aG.valueAsDegree}°`;
    text_angle.style.fontSize = '1cm';
    text_angle.style.dominantBaseline = 'text-after-edge';
    text_angle.style.textAnchor = 'middle';
    text_angle.setAttribute('x', `${d.x / 2}`);
    text_angle.setAttribute('y', `${d.y}`);
    const line_g = g.appendChild(
        document.createElementNS('http://www.w3.org/2000/svg', 'line')
    );
    line_g.setAttribute('stroke', 'gray');
    line_g.setAttribute('stroke-width', '2');
    line_g.setAttribute('x1', `0`);
    line_g.setAttribute('y1', `${d.y}`);
    line_g.setAttribute('x2', `${d.x}`);
    line_g.setAttribute('y2', `${d.y}`);

    return g;
}

export function createG(): SVGGElement {
    return document.createElementNS(
        'http://www.w3.org/2000/svg',
        'g'
    );
}
