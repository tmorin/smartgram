import {addEventListener, formToObject} from '@tmorin/udom';
import {calculatePolygon, Polygon} from './geometry';
import {Distance} from './distance';
import {Angle} from './angle';

const template = `
<p>
    <label>
        screen width:
        <input type="number" name="screenWidth" required min="0" value="6" step="0.1">
        centimeter
    </label>
</p>
<p>
    <label>
        square size:
        <input type="number" name="squareSize" required min="0" value="1" step="0.1">
        centimeter
    </label>
</p>
<p>
    <label>
        angle
        <span class="tooltip" aria-label="from ground"><i class="fa fa-info-circle"></i></span>
        :
        <input type="number" name="groundAngle" required min="0" max="89" value="52">
        degree
    </label>
</p>
<button type="reset">
    Reset to initial values
</button>
`;

export interface Properties {
    screenWidth: number
    squareSize: number
    groundAngle: number
}

export class PropertiesUpdatedEvent extends CustomEvent<Polygon> {
    public static PROPERTIES_UPDATED = 'properties-updated';

    constructor(detail: Polygon) {
        super(PropertiesUpdatedEvent.PROPERTIES_UPDATED, {
            bubbles: true,
            cancelable: false,
            detail
        });
    }
}

export class SgPolygonForm extends HTMLElement {

    private readonly form: HTMLFormElement;

    constructor() {
        super();
        this.form = document.createElement('form');
        this.onsubmit = () => (false);
        this.form.innerHTML = template;
        addEventListener(this.form, 'submit,reset,input,change', evt => {
            evt.stopPropagation();
            setTimeout(() => document.dispatchEvent(new PropertiesUpdatedEvent(this.polygon)), 0);
        });
        window.addEventListener('load', () => {
            document.dispatchEvent(new PropertiesUpdatedEvent(this.polygon));
        }, {once: true});
    }

    get polygon() {
        if (this.form.checkValidity()) {
            const data = formToObject(this.form) as Properties;
            return calculatePolygon(
                new Distance(data.screenWidth),
                new Distance(data.squareSize),
                new Angle(data.groundAngle)
            );
        }
    }

    connectedCallback() {
        if (!this.form.parentElement) {
            this.appendChild(this.form);
        }
    }

}

customElements.define('sg-polygon-form', SgPolygonForm);
