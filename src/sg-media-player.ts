import './sg-media-player.scss';
import {addEventListener} from '@tmorin/udom';
import {MediaSelected} from './sg-media-selection';
import {Media} from './media';
import {BoxSize} from './effects';

export class MediaPlayed extends Event {
    public static readonly MEDIA_PLAYED = 'media-played';

    constructor() {
        super(MediaPlayed.MEDIA_PLAYED, {
            cancelable: false,
            bubbles: true
        });
    }
}

export class SgMediaPlayer extends HTMLElement {

    public size: BoxSize = {
        height: 750,
        width: 750
    };
    private media: Media;
    private readonly canvas: HTMLCanvasElement;
    private playing = false;
    private handlers: Array<() => void> = [];

    constructor() {
        super();
        this.canvas = document.createElement('canvas') as HTMLCanvasElement;
        this.canvas.width = 0;
        this.canvas.height = 0;
    }

    get stream(): MediaStream {
        try {
            return this.canvas['captureStream'](30);
        } catch (e) {
            console.trace('canvas not initialized', e.message);
            return null;
        }
    }

    connectedCallback() {
        if (!this.canvas.parentElement) {
            this.appendChild(this.canvas);
        }
        this.handlers.push(
            addEventListener(document, MediaSelected.MEDIA_SELECTED, (evt: MediaSelected) => this.play(evt.detail))
        );
    }

    disconnectedCallback() {
        this.handlers.forEach(h => h());
    }

    public checkStream() {
        if (typeof this.canvas['captureStream'] !== 'function') {
            throw new Error('The browser do not support this functionality!');
        }
    }

    public play(media: Media) {
        if (this.media) {
            this.media.element.remove();
        }
        this.media = media;
        this.appendChild(this.media.element);
        this.media.element.style.position = 'absolute';
        this.media.element.style.top = '0';
        this.media.element.style.left = '0';
        this.media.element.style.width = '1px';
        this.media.element.style.height = '1px';

        const render = () => {
            const boxWidth = this.canvas.parentElement.getBoundingClientRect().width;
            const boxHeight = this.canvas.parentElement.getBoundingClientRect().height;
            const smallestLength = boxWidth > boxHeight ? boxHeight : boxWidth;
            this.canvas.style.width = `${smallestLength}px`;
            this.canvas.style.height = `${smallestLength}px`;

            media.render(this.canvas, this.size);

            if (this.playing) {
                window.requestAnimationFrame(render);
            } else {
                this.canvas.width = 0;
                this.canvas.height = 0;
            }
        };

        window.requestAnimationFrame(render);

        this.playing = true;
        this.dispatchEvent(new MediaPlayed());
    }

    fullscreen() {
        if (typeof this.canvas['requestFullscreen'] === 'function') {
            this.canvas['requestFullscreen']();
        } else if (typeof this.canvas['webkitRequestFullscreen'] === 'function') {
            this.canvas['webkitRequestFullscreen']();
        } else if (typeof this.canvas['mozRequestFullScreen'] === 'function') {
            this.canvas['mozRequestFullScreen']();
        } else if (typeof this.canvas['msRequestFullscreen'] === 'function') {
            this.canvas['msRequestFullscreen']();
        } else {
            alert('Sorry, fullscreen is not available on your device.')
        }
    }
}

customElements.define('sg-media-player', SgMediaPlayer);
