export enum AngleUnit {
    degree = 'degree',
    radiant = 'radiant'
}
