if ('serviceWorker' in navigator) {
    navigator.serviceWorker
        .register('service-worker.js')
        .then(() => console.log('service-worker registered'))
        .catch(error => console.warn('unable to register service-worker', error));
}