import {Media} from './media';

export abstract class Effect {
    abstract apply(output: HTMLCanvasElement, availableSize: BoxSize)
}

export interface CropProps {
    x: number
    y: number
    w: number
    h: number
}

export interface BoxSize {
    width: number
    height: number
}

export class CropEffect extends Effect {
    constructor(
        public props: CropProps,
        private media: Media
    ) {
        super();
    }

    get max(): CropProps {
        return {
            x: this.media.originalWidth - 1,
            y: this.media.originalHeight - 1,
            w: this.media.originalWidth - this.props.x,
            h: this.media.originalHeight - this.props.y
        };
    }

    apply(canvas: HTMLCanvasElement, boxSize: BoxSize) {
        const available = boxSize.width > boxSize.height ? boxSize.height : boxSize.width;
        const actual = this.media.width + this.media.height * 2;
        const ratio = actual / available;
        const newMediaWidth = Math.floor(this.media.width / ratio);
        const newMediaHeight = Math.floor(this.media.height / ratio);
        const newActual = newMediaWidth + newMediaHeight * 2;

        canvas.width = newActual;
        canvas.height = newActual;

        const ctx = canvas.getContext('2d');

        ctx.rect(0, 0, newActual, newActual);
        ctx.fillStyle = 'black';
        ctx.fill();

        ctx.translate(newMediaHeight, 0);
        ctx.drawImage(
            this.media.element,
            this.props.x, this.props.y,
            this.props.w, this.props.h,
            0, 0,
            newMediaWidth, newMediaHeight,
        );

        ctx.translate(newMediaWidth, newMediaHeight);
        ctx.rotate(90 * Math.PI / 180);
        ctx.drawImage(
            this.media.element,
            this.props.x, this.props.y,
            this.props.w, this.props.h,
            0, -newMediaHeight,
            newMediaWidth, newMediaHeight
        );

        ctx.translate(newMediaWidth, newMediaWidth);
        ctx.rotate(90 * Math.PI / 180);
        ctx.drawImage(
            this.media.element,
            this.props.x, this.props.y,
            this.props.w, this.props.h,
            -newMediaWidth, -newMediaHeight,
            newMediaWidth, newMediaHeight
        );

        ctx.translate(0, newMediaWidth);
        ctx.rotate(90 * Math.PI / 180);
        ctx.drawImage(
            this.media.element,
            this.props.x, this.props.y,
            this.props.w, this.props.h,
            -newMediaWidth, -newMediaHeight,
            newMediaWidth, newMediaHeight
        );

        ctx.resetTransform();

    }
}