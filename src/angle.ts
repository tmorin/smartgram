import {AngleUnit} from './units';

export class Angle {
    constructor(
        private value: number = 0,
        public type: AngleUnit = AngleUnit.degree
    ) {
    }

    get valueAsRadiant() {
        if (this.type === AngleUnit.degree) {
            return this.value * Math.PI / 180;
        }
        return this.value;
    }

    get valueAsDegree() {
        if (this.type === AngleUnit.radiant) {
            return this.value * 180 / Math.PI;
        }
        return this.value;
    }
}