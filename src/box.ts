import {Point} from './point';
import {Distance} from './distance';

export class Box {

    private readonly p1: Point = new Point(0, 0);

    constructor(
        private p2: Point = new Point(0, 0)
    ) {
    }

    get width(): Distance {
        return new Distance(this.p2.x);
    }

    get height(): Distance {
        return new Distance(this.p2.y);
    }

    resize(width: Distance, height: Distance) {
        return new Box(
            this.p1.translateX(width.value).translateY(height.value)
        );
    }

    scaleTo(scale: number) {
        return new Box(
            this.p2.scaleTo(scale)
        );
    }

}