import {IoSignalOptions} from './webrtc/signal';

export interface Config {
    io?: IoSignalOptions
    rtc?: RTCConfiguration
}

const RTC_CONFIGURATION: RTCConfiguration = {
    iceServers: [
        {urls: 'stun:stun.stunprotocol.org:3478'}
    ]
};

export function getConfig(): Config {
    if (location.hostname === 'tmorin.gitlab.io') {
        return {
            io: {
                url: 'signal.smartgram.morin.io'
            },
            rtc: RTC_CONFIGURATION
        };
    }
    return {
        io: {
            url: location.origin,
            opts: {
                path: '/signal/socket.io'
            }
        },
        rtc: RTC_CONFIGURATION
    };
}