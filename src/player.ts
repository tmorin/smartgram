import 'document-register-element';
import './player.scss';
import {Session} from './webrtc/session';
import {IoSignal} from './webrtc/signal';
import {ConsumeOnlyHandler} from './webrtc/consume-only';
import {getConfig} from './config';
import {addDelegatedEventListener} from '@tmorin/udom';
import './pwa';

const config = getConfig();

function renderOpenRoomForm() {
    main.innerHTML = `
        <form name="open-room">
            <input name="roomId" autocomplete="off" placeholder="type there the roomId">
            <button type="submit">start</button>
        </form>
    `;
}

function renderSpinner() {
    main.innerHTML = `
        <div style="text-align: center">
        <i class="fa fa-fw fa-spinner fa-spin fa-6x"></i>
        <div>waiting stream</div>
        </div>
    `;
}

function renderVideo(streams: MediaStream[]) {
    main.innerHTML = '';
    const video = main.appendChild(document.createElement('video')) as HTMLVideoElement;
    video.autoplay = true;
    video.srcObject = streams[0];
}

const main = document.querySelector('main') as HTMLMainElement;
addDelegatedEventListener(main, 'form[name=open-room]', 'submit', (evt, form: HTMLFormElement) => {
    evt.preventDefault();
    const roomId = (form.elements.namedItem('roomId') as HTMLInputElement).value.trim();
    if (roomId) {
        location.search = `?roomId=${roomId}`;
    }
});

addDelegatedEventListener(document.body, 'header button[data-action=fullscreen]', 'click', () => {
    if (typeof main['requestFullscreen'] === 'function') {
        main['requestFullscreen']();
    } else if (typeof this.canvas['webkitRequestFullscreen'] === 'function') {
        main['webkitRequestFullscreen']();
    } else if (typeof this.canvas['mozRequestFullScreen'] === 'function') {
        main['mozRequestFullScreen']();
    } else if (typeof this.canvas['msRequestFullscreen'] === 'function') {
        main['msRequestFullscreen']();
    } else {
        alert('Sorry, fullscreen is not available on your device.')
    }
});

window.addEventListener('load', async () => {

    const queryParams: { [k: string]: any } = location.search
        .replace(/^\?/, '')
        .split('&')
        .reduce((result, e) => {
            const sIndex = e.indexOf('=', 0);
            const key = e.substring(0, sIndex);
            result[key] = decodeURI(e.substring(sIndex + 1));
            return result;
        }, {});


    if (queryParams.roomId) {
        renderSpinner();
        try {
            const opts = {
                rtc: config.rtc,
                consume: {
                    audio: false,
                    video: true
                }
            };
            const signal = new IoSignal(Object.assign(
                {
                    roomId: queryParams.roomId
                },
                config.io
            ));
            const consumer = Session.start(new ConsumeOnlyHandler(opts, signal));
            consumer.addListener('streams-ready', renderVideo);
        } catch (e) {
            console.error('unable to consume')
        }
    } else {
        renderOpenRoomForm();
    }
});
