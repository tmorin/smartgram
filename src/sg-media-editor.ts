import './sg-media-editor.scss';
import {addDelegatedEventListener, addEventListener, formToObject} from '@tmorin/udom';
import {MediaSelected} from './sg-media-selection';
import {Media} from './media';
import {CropProps} from './effects';

const IncrementalDOM = require('incremental-dom');

// @ts-ignore
const template = idomizer`
<tpl-if expression="data.media">
    <h3>2. Tweak it</h3>
    <p>
        <dl class="inline">
            <dt>type</dt>
            <dd class="type">{{ data.media.type }}</dd>
            <dt>width</dt>
            <dd class="width">{{ data.media.width }}</dd>
            <dt>height</dt>
            <dd class="height">{{ data.media.height }}</dd>
        </dl>
    </p>
    <form name="crop" class="row" tpl-key="Date.now()">
        <p class="col-md-12 col-lg-12">Crop the media to maximize.</p>
        <div class="col-md-12 col-lg-6">
            <p>x <label><input name="x" type="number" value="{{data.media.crop.props.x}}" min="0" max="{{data.media.crop.max.x}}"></label></p>
            <p>y <label><input name="y" type="number" value="{{data.media.crop.props.y}}" min="0" max="{{data.media.crop.max.y}}"></label></p>
        </div>
        <div class="col-md-12 col-lg-6">
            <p>width <label><input name="w" type="number" value="{{data.media.crop.props.w}}" min="0" max="{{data.media.crop.max.w}}"></label></p>
            <p>height <label><input name="h" type="number" value="{{data.media.crop.props.h}}" min="0" max="{{data.media.crop.max.h}}"></label></p>
        </div>
    </form>
</tpl-if>
`;

export class SgMediaEditor extends HTMLElement {

    private media: Media;

    constructor() {
        super();
        addEventListener(document, MediaSelected.MEDIA_SELECTED, (evt: MediaSelected) => {
            this.startEdition(evt.detail);
        });
        addDelegatedEventListener(this, 'form[name=crop]', 'change,input', async (evt, form: HTMLFormElement) => {
            const props = formToObject(form) as CropProps;
            const x = form.elements.namedItem('x') as HTMLInputElement;
            x.setCustomValidity('');
            const w = form.elements.namedItem('w') as HTMLInputElement;
            w.setCustomValidity('');
            const y = form.elements.namedItem('y') as HTMLInputElement;
            y.setCustomValidity('');
            const h = form.elements.namedItem('h') as HTMLInputElement;
            h.setCustomValidity('');
            if (props.x + props.w > this.media.originalWidth) {
                const msg = `x + y > media width, i.e. (${x.value}+${w.value}=${x.valueAsNumber + w.valueAsNumber} px) > ${this.media.originalWidth} px`;
                x.setCustomValidity(msg);
                w.setCustomValidity(msg);
            }
            if (props.y + props.h > this.media.originalWidth) {
                const msg = `y + h > media height, i.e. (${y.value}+${h.value}=${y.valueAsNumber + h.valueAsNumber} px) > ${this.media.originalHeight} px`;
                y.setCustomValidity(msg);
                h.setCustomValidity(msg);
            }
            if (form.checkValidity()) {
                this.media.crop.props = props;
            }
        });
    }

    connectedCallback() {
        this.render();
    }

    startEdition(media: Media) {
        this.classList.add('editing');
        this.media = media;
        this.render();
    }

    private render() {
        IncrementalDOM.patch(this, template(IncrementalDOM), this);
    }

}

customElements.define('sg-media-editor', SgMediaEditor);
