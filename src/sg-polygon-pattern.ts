import {Polygon} from './geometry';
import {Figure, PatternFigureFactory} from './figure';
import {PropertiesUpdatedEvent} from './sg-polygon-form';

const templateUl = `
<li>
    <i class="fa fa-fw fa-download"></i>
    <a href="#" name="download">download</a>
</li>
`;

export class SgPolygonPattern extends HTMLElement {

    public unit = 'cm';

    public margin = 0.5;

    private readonly factor = 100;

    private readonly ul: HTMLUListElement;

    private figure: Figure;

    constructor() {
        super();
        this.ul = document.createElement('ul');
        document.addEventListener(
            PropertiesUpdatedEvent.PROPERTIES_UPDATED,
            (evt: PropertiesUpdatedEvent) => {
                this.polygon = evt.detail;
            }
        );
    }

    set polygon(value: Polygon) {
        if (!value) {
            if (this.figure) {
                this.figure.remove();
                this.figure = null;
            }
            return;
        }

        const oldFigure = this.figure;
        this.figure = PatternFigureFactory.get(value)
            .unit(this.unit)
            .margin(this.margin)
            .factor(this.factor)
            .build(this);

        if (oldFigure) {
            oldFigure.remove();
        }

        const imageAsBase64 = this.figure.toString();
        const top = value.a.distanceTo(value.b).value;
        const height = Math.floor(value.c.y * this.factor) / this.factor;
        const a = this.querySelector('a[name=download]');
        a.setAttribute('href', 'data:image/svg;charset=utf-8,' + imageAsBase64);
        a.setAttribute('download', `pattern-${value.c.x}x${top}x${height}.svg`);
    }

    connectedCallback() {
        if (!this.ul.parentElement) {
            this.appendChild(this.ul);
            this.ul.innerHTML = templateUl;
            this.ul.classList.add('no-print');
            this.ul.classList.add('inline');
        }
    }

}

customElements.define('sg-polygon-pattern', SgPolygonPattern);
