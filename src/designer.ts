import 'document-register-element';
import {addDelegatedEventListener} from '@tmorin/udom';
import './designer.scss';
import './sg-media-player';
import './sg-media-selection';
import './sg-media-editor';
import './sg-media-provider';
import {SgMediaPlayer} from './sg-media-player';
import {MediaSelected} from './sg-media-selection';
import './pwa';

addDelegatedEventListener(document.body, 'header button[data-action=fullscreen]', 'click', () => {
    (document.getElementById('player') as SgMediaPlayer).fullscreen();
});

let editing = false;
document.addEventListener(MediaSelected.MEDIA_SELECTED, () => {
    editing = true;
});
window.onbeforeunload = () => editing || undefined;
