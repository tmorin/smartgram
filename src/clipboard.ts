export function copyTextToClipboard(text: string) {
    const textarea = document.createElement('textarea') as HTMLTextAreaElement;
    textarea.style.position = 'fixed';
    textarea.style.top = '0';
    textarea.style.left = '0';
    textarea.style.width = '1px';
    textarea.style.height = '1px';
    textarea.style.padding = '0';
    textarea.style.border = 'none';
    textarea.style.outline = 'none';
    textarea.style.boxShadow = 'none';
    textarea.style.background = 'transparent';
    document.body.appendChild(textarea);

    textarea.value = text;
    textarea.select();

    try {
        if (!document.execCommand('copy')) {
            console.trace('unable to copy [%s]', text);
        }
    } catch (err) {
        console.error('Oops, unable to copy', err);
    }

    document.body.removeChild(textarea);
}
