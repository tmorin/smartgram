import {Polygon} from './geometry';
import {OverviewFigureFactory} from './figure';
import {PropertiesUpdatedEvent} from './sg-polygon-form';

export class SgPolygonOverview extends HTMLElement {

    public unit = 'cm';

    public margin = 0.5;

    private readonly factor = 100;

    constructor() {
        super();
        document.addEventListener(
            PropertiesUpdatedEvent.PROPERTIES_UPDATED,
            (evt: PropertiesUpdatedEvent) => {
                this.polygon = evt.detail;
            }
        );
    }

    set polygon(value: Polygon) {
        this.innerHTML = '';

        if (!value) {
            return;
        }

        OverviewFigureFactory.get(value)
            .unit(this.unit)
            .margin(this.margin)
            .factor(this.factor)
            .build(this);
    }

}

customElements.define('sg-polygon-overview', SgPolygonOverview);
