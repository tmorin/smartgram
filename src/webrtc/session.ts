import {EventEmitter} from 'events';
import {Peer} from './peer';
import {Handler} from './handler';

export interface SessionEventMap {
    'joined': void
    'error': Error
    'peer-joined': Peer
    'peer-left': Peer
    'peer-destroyed': Peer
    'peer-state-changed': Peer
    'streams-ready': ReadonlyArray<MediaStream>
}

export interface Session extends EventEmitter {
    addListener<K extends keyof SessionEventMap>(event: K, listener: (ev: SessionEventMap[K]) => void): this;

    emit<K extends keyof SessionEventMap>(event: K, arg?: SessionEventMap[K]): boolean;
}

export class Session extends EventEmitter {

    private readonly handlers: Array<() => void> = [];

    private constructor(
        private readonly handler: Handler
    ) {
        super();

        this.signal.join();

        this.signal.addListener('joined', async () => {
            this.emit('joined');
        });

        this.signal.addListener('peer-joined', async evt => {
            try {
                const peer = await this.createPeer(evt.from);
                if (peer) {
                    await this.handler.onPeerJoined(peer);
                }
                this.emit('peer-joined', peer);
            } catch (e) {
                console.warn('unable to handle peer-joined', e.message);
                this.emit('error', e);
            }
        });

        this.signal.addListener('offer-received', async evt => {
            try {
                const peer = await this.createPeer(evt.from);
                await this.handler.onOfferReceived(peer, evt.desc);
            } catch (e) {
                console.warn('unable to handle offer-received', e.message);
                this.emit('error', e);
            }
        });

        this.signal.addListener('answer-received', async evt => {
            try {
                if (this.peers.has(evt.from)) {
                    await this.handler.onAnswerReceived(this.peers.get(evt.from), evt.desc);
                }
            } catch (e) {
                console.warn('unable to handle answer-received', e.message);
                this.emit('error', e);
            }
        });

        this.signal.addListener('candidate-received', async evt => {
            try {
                if (this.peers.has(evt.from)) {
                    await this.handler.onCandidateReceived(this.peers.get(evt.from), evt.candidate);
                }
            } catch (e) {
                console.warn('unable to handle candidate-received', e.message);
                this.emit('error', e);
            }
        });

        this.signal.addListener('peer-left', async evt => {
            try {
                if (this.peers.has(evt.from)) {
                    const peer = this.peers.get(evt.from);
                    await this.handler.onPeerLeft(peer);
                    this.emit('peer-left', peer);
                }

            } catch (e) {
                console.warn('unable to handle peer-left', e.message);
                this.emit('error', e);
            }
        });

        window.addEventListener('beforeunload', () => {
            this.leave();
        });
    }

    public get clientId() {
        return this.handler.signal.clientId;
    }

    public get roomId() {
        return this.handler.signal.roomId;
    }

    public get peers() {
        return this.handler.peers;
    }

    private get signal() {
        return this.handler.signal;
    }

    public static start(handler: Handler): Session {
        return new Session(handler);
    }

    leave() {
        this.handler.destroy();
        this.handlers.forEach(h => h());
    }

    private async createPeer(peerId: string): Promise<Peer> {
        const peer = await this.handler.createPeer(peerId);
        if (peer) {
            peer.addListener('icecandidate', async (evt: RTCPeerConnectionEventMap['icecandidate']) => {
                try {
                    if (evt.candidate) {
                        await this.handler.onCandidateReady(peer, evt.candidate);
                    }
                } catch (e) {
                    console.warn('unable to handle icecandidate-ready', e.message);
                    this.emit('error', e);
                }
            });
            peer.addListener('negotiationneeded', async () => {
                try {
                    await this.handler.onNegotiationNeeded(peer);
                } catch (e) {
                    console.warn('unable to handle negotiation-needed', e.message);
                    this.emit('error', e);
                }
            });
            peer.addListener('track', (evt: RTCPeerConnectionEventMap['track']) => {
                this.emit('streams-ready', evt.streams);
            });
            peer.addListener('connectionstatechange', (evt: RTCPeerConnectionEventMap['connectionstatechange']) => {
                console.log('connectionstatechange', peer.peerId, peer.rtc.connectionState);
                this.emit('peer-state-changed', peer);
            });
            peer.addListener('destroyed', () => {
                this.peers.delete(peer.peerId);
                this.emit('peer-destroyed', peer);
            });
        }
        return peer;
    }

}