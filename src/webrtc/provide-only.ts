import {Signal} from './signal';
import {Peer} from './peer';
import {Handler} from './handler';

export interface ProvideOnlyHandlerOptions {
    rtc: RTCConfiguration
    stream: MediaStream
}

export class ProvideOnlyHandler extends Handler {

    constructor(
        private readonly opts: ProvideOnlyHandlerOptions,
        signal: Signal,
        peers: Map<string, Peer> = new Map<string, Peer>()
    ) {
        super(signal, peers);
    }

    async createPeer(peerId: string): Promise<Peer> {
        if (this.peers.has(peerId)) {
            this.peers.get(peerId).destroy();
        }
        const peer = new Peer(
            peerId,
            new RTCPeerConnection(this.opts.rtc)
        );
        this.peers.set(peerId, peer);
        return peer;
    }

    async createOffer(peer: Peer): Promise<RTCSessionDescriptionInit> {
        return peer.rtc.createOffer();
    }

    async createAnswer(peer: Peer): Promise<RTCSessionDescriptionInit> {
        return peer.rtc.createAnswer();
    }

    async onPeerJoined(peer: Peer): Promise<void> {
        if (this.opts.stream) {
            this.opts.stream.getTracks().forEach(t => peer.rtc.addTrack(t, this.opts.stream));
        } else {
            const desc = await this.createOffer(peer);
            await peer.rtc.setLocalDescription(desc);
            this.signal.sendOffer(peer.peerId, desc);
        }
    }

    async onOfferReceived(peer: Peer, desc: RTCSessionDescriptionInit): Promise<void> {
        await peer.rtc.setRemoteDescription(desc);
        if (this.opts.stream) {
            this.opts.stream.getTracks().forEach(t => peer.rtc.addTrack(t, this.opts.stream));
        }
        const answer = await this.createAnswer(peer);
        await peer.rtc.setLocalDescription(answer);
        this.signal.sendAnswer(peer.peerId, answer);
    }

    async onAnswerReceived(peer: Peer, desc: RTCSessionDescriptionInit): Promise<void> {
        await peer.rtc.setRemoteDescription(desc);
    }

    async onNegotiationNeeded(peer: Peer): Promise<void> {
        const desc = await this.createOffer(peer);
        await peer.rtc.setLocalDescription(desc);
        this.signal.sendOffer(peer.peerId, desc);
    }

}