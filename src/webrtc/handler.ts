import {Signal} from './signal';
import {Peer} from './peer';

export abstract class Handler {

    protected constructor(
        public readonly signal: Signal,
        public readonly peers: Map<string, Peer> = new Map<string, Peer>()
    ) {
    }

    abstract async createPeer(peerId: string): Promise<Peer>

    abstract async createOffer(peer: Peer): Promise<RTCSessionDescriptionInit>

    abstract async createAnswer(peer: Peer): Promise<RTCSessionDescriptionInit>

    abstract async onPeerJoined(peer: Peer): Promise<void>

    abstract async onNegotiationNeeded(peer: Peer): Promise<void>

    abstract async onOfferReceived(peer: Peer, desc: RTCSessionDescriptionInit): Promise<void>

    abstract async onAnswerReceived(peer: Peer, desc: RTCSessionDescriptionInit): Promise<void>

    async onCandidateReady(peer: Peer, desc: RTCIceCandidateInit | RTCIceCandidate): Promise<void> {
        this.signal.sendCandidate(peer.peerId, desc)
    }

    async onCandidateReceived(peer: Peer, candidate: RTCIceCandidateInit | RTCIceCandidate): Promise<void> {
        if (peer.rtc.remoteDescription) {
            await peer.rtc.addIceCandidate(candidate)
        }
    }

    async onPeerLeft(peer: Peer): Promise<void> {
        peer.destroy();
    }

    destroy() {
        this.signal.destroy();
        this.peers.forEach(p => p.destroy());
    }
}

