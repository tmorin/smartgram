import {EventEmitter} from 'events';
import * as io from 'socket.io-client';

export interface Signal extends EventEmitter {

    readonly clientId: string

    readonly roomId: string

    join()

    sendOffer(to: string, desc: RTCSessionDescriptionInit)

    sendAnswer(to: string, desc: RTCSessionDescriptionInit)

    sendCandidate(to: string, candidate: RTCIceCandidateInit | RTCIceCandidate)

    destroy()

}

export interface IoSignalOptions {
    roomId?: string
    url?: string
    opts?: SocketIOClient.ConnectOpts
}

export class IoSignal extends EventEmitter implements Signal {

    public readonly socket: SocketIOClient.Socket;

    constructor(
        private readonly options: IoSignalOptions
    ) {
        super();
        this.socket = io(options.url, Object.assign({forceNew: true}, options.opts, {
            autoConnect: false,
            query: {
                roomId: options.roomId || ''
            }
        }));
    }

    get roomId() {
        return this.options.roomId || this.socket.id;
    }

    get clientId() {
        return this.socket.id;
    }

    join() {
        if (this.socket.connected) {
            return;
        }

        console.log('Session', 'join', this.socket.nsp);
        this.socket.open();

        this.socket.on('connect', () => {
            console.log('Signal', 'joined', this.socket.id);
            this.emit('joined');
        });

        this.socket.on('peer-joined', (evt) => {
            console.log('Signal', 'peer-joined', evt);
            this.emit('peer-joined', evt);
        });

        this.socket.on('offer-received', (evt) => {
            if (evt.to === this.socket.id) {
                console.log('Signal', 'offer-received', evt);
                this.emit('offer-received', evt);
            }
        });

        this.socket.on('answer-received', (evt) => {
            if (evt.to === this.socket.id) {
                console.log('Signal', 'answer-received', evt);
                this.emit('answer-received', evt);
            }
        });

        this.socket.on('candidate-received', (evt) => {
            if (evt.to === this.socket.id) {
                console.log('Signal', 'candidate-received');
                this.emit('candidate-received', evt);
            }
        });

        this.socket.on('peer-left', (evt) => {
            console.log('Signal', 'peer-left', evt);
            this.emit('peer-left', evt);
        });
    }

    sendOffer(to: string, desc: RTCSessionDescriptionInit) {
        if (this.socket.disconnected) {
            return;
        }
        console.log('Signal', 'sendOffer', to);
        this.socket.emit('send-offer', {from: this.socket.id, to, desc});
    }

    sendAnswer(to: string, desc: RTCSessionDescriptionInit) {
        if (this.socket.disconnected) {
            return;
        }
        console.log('Signal', 'sendAnswer', to);
        this.socket.emit('send-answer', {from: this.socket.id, to, desc});
    }

    sendCandidate(to: string, candidate: RTCIceCandidateInit | RTCIceCandidate) {
        if (this.socket.disconnected) {
            return;
        }
        console.log('Signal', 'sendCandidate', to);
        this.socket.emit('send-candidate', {from: this.socket.id, to, candidate});
    }

    leave() {
        if (this.socket.disconnected) {
            return;
        }
        console.log('Signal', 'leave');
        this.socket.emit('leave', {from: this.socket.id});
    }

    destroy() {
        this.leave();
        this.removeAllListeners();
        this.socket.removeAllListeners();
        this.socket.close();
        this.emit('destroyed');
    }

}