import {Signal} from './signal';
import {Peer} from './peer';
import {Handler} from './handler';

export interface ConsumeOnlyHandlerOptions {
    rtc: RTCConfiguration
    consume?: {
        video: boolean,
        audio: boolean
    }
}

export class ConsumeOnlyHandler extends Handler {

    constructor(
        private readonly opts: ConsumeOnlyHandlerOptions,
        signal: Signal,
        peers: Map<string, Peer> = new Map<string, Peer>()
    ) {
        super(signal, peers);
    }

    async createPeer(peerId: string): Promise<Peer> {
        if (peerId === this.signal.roomId) {
            this.peers.forEach(p => p.destroy());
            const peer = new Peer(
                peerId,
                new RTCPeerConnection(this.opts.rtc)
            );
            this.peers.set(peerId, peer);
            return peer;
        }
    }

    async createOffer(peer: Peer): Promise<RTCSessionDescriptionInit> {
        if (this.opts.consume) {
            return peer.rtc.createOffer({
                offerToReceiveVideo: this.opts.consume.video,
                offerToReceiveAudio: this.opts.consume.audio
            });
        }
        return peer.rtc.createOffer();
    }

    async createAnswer(peer: Peer): Promise<RTCSessionDescriptionInit> {
        if (this.opts.consume) {
            return peer.rtc.createAnswer({
                offerToReceiveVideo: this.opts.consume.video,
                offerToReceiveAudio: this.opts.consume.audio
            });
        }
        return peer.rtc.createAnswer();
    }

    async onPeerJoined(peer: Peer): Promise<void> {
        const desc = await this.createOffer(peer);
        await peer.rtc.setLocalDescription(desc);
        this.signal.sendOffer(peer.peerId, desc);
    }

    async onOfferReceived(peer: Peer, desc: RTCSessionDescriptionInit): Promise<void> {
        await peer.rtc.setRemoteDescription(desc);
        const answer = await this.createAnswer(peer);
        await peer.rtc.setLocalDescription(answer);
        this.signal.sendAnswer(peer.peerId, answer);
    }

    async onAnswerReceived(peer: Peer, desc: RTCSessionDescriptionInit): Promise<void> {
        await peer.rtc.setRemoteDescription(desc);
    }

    async onNegotiationNeeded(peer: Peer): Promise<void> {
        const desc = await this.createOffer(peer);
        await peer.rtc.setLocalDescription(desc);
        this.signal.sendOffer(peer.peerId, desc);
    }

}