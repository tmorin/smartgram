import {EventEmitter} from 'events';
import {addEventListener} from '@tmorin/udom';

export interface PeerEventMap extends RTCPeerConnectionEventMap {
    'destroyed': void
}

export interface Peer extends EventEmitter {
    addListener<K extends keyof PeerEventMap>(event: K, listener: (ev: PeerEventMap[K]) => void): this;

    emit<K extends keyof PeerEventMap>(event: K, arg?: PeerEventMap[K]): boolean;
}

export class Peer extends EventEmitter {

    public readonly handlers: Array<() => void> = [];

    public constructor(
        public readonly peerId: string,
        public readonly rtc: RTCPeerConnection
    ) {
        super();
        this.handlers.push(
            addEventListener(this.rtc, 'connectionstatechange', this.onConnectionStateChange.bind(this)),
            addEventListener(this.rtc, 'datachannel', this.onDataChannel.bind(this)),
            addEventListener(this.rtc, 'icecandidate', this.onIceCandidate.bind(this)),
            addEventListener(this.rtc, 'icecandidateerror', this.onIceCandidateError.bind(this)),
            addEventListener(this.rtc, 'iceconnectionstatechange', this.onIceConnectionStateChange.bind(this)),
            addEventListener(this.rtc, 'icegatheringstatechange', this.onIceGatheringStateChange.bind(this)),
            addEventListener(this.rtc, 'signalingstatechange', this.onSignalingStateChange.bind(this)),
            addEventListener(this.rtc, 'statsended', this.onStatsEnded.bind(this)),
            addEventListener(this.rtc, 'track', this.onTrack.bind(this)),
            addEventListener(this.rtc, 'negotiationneeded', this.onNegotiationNeeded.bind(this)),
        );
    };

    destroy() {
        console.log('Peer', this.peerId, 'destroyed');
        this.emit('destroyed');
        this.removeAllListeners();
        this.handlers.forEach(h => h());
        this.rtc.close();
    }

    private onConnectionStateChange(evt: RTCPeerConnectionEventMap['connectionstatechange']) {
        this.emit('connectionstatechange', evt);
    }

    private onDataChannel(evt: RTCPeerConnectionEventMap['datachannel']) {
        this.emit('datachannel', evt);
    }

    private onIceCandidate(evt: RTCPeerConnectionEventMap['icecandidate']) {
        this.emit('icecandidate', evt);
    }

    private onIceCandidateError(evt: RTCPeerConnectionEventMap['icecandidateerror']) {
        this.emit('icecandidateerror', evt);
    }

    private onIceConnectionStateChange(evt: RTCPeerConnectionEventMap['iceconnectionstatechange']) {
        this.emit('iceconnectionstatechange', evt);
    }

    private onIceGatheringStateChange(evt: RTCPeerConnectionEventMap['icegatheringstatechange']) {
        this.emit('icegatheringstatechange', evt);
    }

    private onNegotiationNeeded(evt: RTCPeerConnectionEventMap['negotiationneeded']) {
        this.emit('negotiationneeded', evt);
    }

    private onSignalingStateChange(evt: RTCPeerConnectionEventMap['signalingstatechange']) {
        this.emit('signalingstatechange', evt);
    }

    private onStatsEnded(evt: RTCPeerConnectionEventMap['statsended']) {
        this.emit('statsended', evt);
    }

    private onTrack(evt: RTCPeerConnectionEventMap['track']) {
        this.emit('track', evt);
    }

}