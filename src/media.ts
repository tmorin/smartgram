import {addEventListener} from '@tmorin/udom';
import {BoxSize, CropEffect} from './effects';

export type MediaElement = HTMLImageElement | HTMLVideoElement | HTMLCanvasElement;

export class Media {

    public readonly originalWidth;

    public readonly originalHeight;

    public readonly type;

    public readonly crop: CropEffect;

    constructor(
        public readonly element: MediaElement
    ) {
        if (this.element instanceof HTMLVideoElement) {
            this.originalWidth = this.element.videoWidth;
        } else if (this.element) {
            this.originalWidth = this.element.width as number;
        } else {
            this.originalWidth = 0;
        }

        if (this.element instanceof HTMLVideoElement) {
            this.originalHeight = this.element.videoHeight;
        } else if (this.element) {
            this.originalHeight = this.element.height as number;
        } else {
            this.originalHeight = 0;
        }

        this.type = this.element.tagName.toLowerCase();
        this.crop = new CropEffect({
            x: 0,
            y: 0,
            w: this.originalWidth,
            h: this.originalHeight
        }, this);
    }

    get width(): number {
        if (this.crop) {
            return this.crop.props.w;
        }
        return this.originalWidth;
    }

    get height(): number {
        if (this.crop) {
            return this.crop.props.h;
        }
        return this.originalHeight;
    }

    public render(output: HTMLCanvasElement, size: BoxSize) {
        try {
            this.crop.apply(output, size);
        } catch (e) {
            console.log(e.name)
        }
    }
}

export async function loadImg(src: string): Promise<Media> {
    return new Promise<Media>((resolve, reject) => {
        const image = document.createElement('img') as HTMLImageElement;
        const u1 = addEventListener(image, 'load', () => {
            u2();
            resolve(new Media(image));
        }, {once: true});
        const u2 = addEventListener(image, 'error', () => {
            u1();
            reject(new Error(`unable to load the image ${src}`));
        }, {once: true});
        image.src = src;
    });
}

export async function loadVideo(src: string): Promise<Media> {
    return new Promise<Media>((resolve, reject) => {
        const video = document.createElement('video') as HTMLVideoElement;
        video.muted = true;
        video.preload = 'auto';
        video.loop = true;
        video.autoplay = true;
        video.controls = true;
        video.src = src;
        const handlers = [
            addEventListener(video, 'loadedmetadata', () => {
                handlers.forEach(h => h());
                resolve(new Media(video));
            }, {once: true}),
            addEventListener(video, 'error', () => {
                handlers.forEach(h => h());
                reject(new Error(`unable to load the video ${src}`));
            }, {once: true})
        ];
        video.load();
    });
}
