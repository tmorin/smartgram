import {Distance} from './distance';

export class Point {

    constructor(
        public x: number = 0,
        public y: number = 0
    ) {
        this.x = x;
        this.y = y;
    }

    translateX(x: number) {
        return new Point(
            this.x + x,
            this.y
        );
    }

    translateY(y: number) {
        return new Point(
            this.x,
            this.y + y
        );
    }

    distanceTo(p: Point) {
        return new Distance(
            Math.hypot((p.x - this.x), (p.y - this.y))
        );
    }

    scaleTo(value: number = 1) {
        return new Point(
            this.x * value,
            this.y * value
        )
    }

}