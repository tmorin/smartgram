export class Distance {

    constructor(
        public readonly value: number
    ) {
    }

    plus(distance: Distance) {
        return new Distance(
            Math.abs(this.value + distance.value));
    }

    minus(distance: Distance) {
        return new Distance(
            Math.abs(this.value - distance.value)
        );
    }

    divide(factor: number = 1) {
        return new Distance(
            this.value * (1 / factor)
        );
    }
}