import {Polygon, scalePolygon} from './geometry';
import {createG, createLegends, createPolygon, createSvg, usePolygon} from './svg';
import {Box} from './box';
import {Point} from './point';

export interface FigureOptions {
    unit: string
    margin: number
    factor: number
    polygon: Polygon
}

export class Figure {

    constructor(
        parent: HTMLElement,
        private options: FigureOptions,
        public svg: SVGSVGElement = createSvg()
    ) {
        parent.appendChild(this.svg);
    }

    resize(): Figure {
        const {margin, factor} = this.options;

        const viewBox = new Box(new Point(
            this.svg.getBBox().width + (margin * 2 * factor),
            this.svg.getBBox().height + (margin * 2 * factor)
        ));
        const svgBox = viewBox.scaleTo(1 / factor);

        this.svg.setAttribute('viewBox', `0 0 ${viewBox.width.value} ${viewBox.height.value}`);
        this.svg.style.width = `${svgBox.width.value}cm`;
        this.svg.style.height = `${svgBox.height.value}cm`;

        return this;
    }

    append<T extends SVGElement>(element: T): T {
        return this.svg.appendChild(element);
    }

    remove() {
        this.svg.remove();
        return this;
    }

    toString() {
        return encodeURIComponent(this.svg.outerHTML);
    }
}

export abstract class FigureFactory {

    protected options: FigureOptions;

    protected constructor(polygon: Polygon) {
        this.options = {
            unit: 'cm',
            margin: 0.5,
            factor: 100,
            polygon
        }
    }

    unit(value: string): FigureFactory {
        this.options.unit = value;
        return this;
    }

    factor(value: number): FigureFactory {
        this.options.factor = value;
        return this;
    }

    margin(value: number): FigureFactory {
        this.options.margin = value;
        return this;
    }

    public abstract build(parent: HTMLElement): Figure;

}

export class OverviewFigureFactory extends FigureFactory {
    public static get(polygon: Polygon) {
        return new OverviewFigureFactory(polygon);
    }

    build(parent: HTMLElement): Figure {
        const {factor, polygon, unit, margin} = this.options;
        const sp = scalePolygon(polygon, factor);

        const figure = new Figure(parent, this.options);

        const g = figure.append(createG());
        g.setAttribute('transform', `translate(${margin * factor},${margin * factor})`);
        g.appendChild(createPolygon(sp));
        g.appendChild(createLegends(sp, factor, unit));

        const g_t_x = margin * factor - g.getBBox().x;
        const g_t_y = margin * factor - g.getBBox().y;
        g.setAttribute('transform', `translate(${g_t_x},${g_t_y})`);

        return figure.resize();
    }
}

export class PatternFigureFactory extends FigureFactory {
    public static get(polygon: Polygon) {
        return new PatternFigureFactory(polygon);
    }

    build(parent: HTMLElement): Figure {
        const {factor, polygon, margin} = this.options;
        const sp = scalePolygon(polygon, factor);
        const {a, b, aB} = sp;
        const dAB = a.distanceTo(b);

        const figure = new Figure(parent, this.options);

        const defs = figure.append(document.createElementNS(
            'http://www.w3.org/2000/svg',
            'defs'
        ));
        defs.appendChild(createPolygon(sp, 'polygon'));

        let rotation = aB.valueAsDegree * 2;
        if (rotation > 90) {
            rotation = 90 + (90 - rotation);
        } else {
            rotation = 90;
        }

        const g1 = figure.append(createG());
        const p1_r = new Point(margin * factor, margin * factor);
        const p1 = g1.appendChild(usePolygon('polygon'));
        p1.setAttribute('x', `${p1_r.x}`);
        p1.setAttribute('y', `${p1_r.y}`);

        const g2 = g1.appendChild(createG());
        const p2_r = p1_r.translateX(dAB.value);
        const p2 = g2.appendChild(usePolygon('polygon'));
        p2.setAttribute('x', `${p2_r.x}`);
        p2.setAttribute('y', `${p2_r.y}`);
        g2.setAttribute('transform', `rotate(${rotation}, ${p2_r.x}, ${p2_r.y})`);

        const g3 = g2.appendChild(createG());
        const p3_r = p2_r.translateX(dAB.value);
        const p3 = g3.appendChild(usePolygon('polygon'));
        p3.setAttribute('x', `${p3_r.x}`);
        p3.setAttribute('y', `${p3_r.y}`);
        g3.setAttribute('transform', `rotate(${rotation}, ${p3_r.x}, ${p3_r.y})`);

        const g4 = g3.appendChild(createG());
        const p4_r = p3_r.translateX(dAB.value);
        const p4 = g4.appendChild(usePolygon('polygon'));
        p4.setAttribute('x', `${p4_r.x}`);
        p4.setAttribute('y', `${p4_r.y}`);
        g4.setAttribute('transform', `rotate(${rotation}, ${p4_r.x}, ${p4_r.y})`);

        const g1_t_x = margin * factor - g1.getBBox().x;
        const g1_t_y = margin * factor - g1.getBBox().y;
        g1.setAttribute('transform', `translate(${g1_t_x},${g1_t_y})`);

        return figure.resize();
    }
}
