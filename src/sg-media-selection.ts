import './sg-media-selection.scss';
import {addDelegatedEventListener} from '@tmorin/udom';
import {loadImg, loadVideo, Media} from './media';

const samples = require('./samples.json') as Array<Sample>;

interface Sample {
    name: string
    type: string
    icon: string
    source: string
    credits: string
}

export class MediaSelected extends CustomEvent<Media> {
    public static readonly MEDIA_SELECTED = 'media-selected';

    constructor(media: Media) {
        super(MediaSelected.MEDIA_SELECTED, {
            cancelable: false,
            bubbles: true,
            detail: media
        });
    }
}

const template = `
    <h3>1. Select a media</h3>
    <div class="samples"></div>
    <p>
        Open a video from the file system
        <input name="video" type="file" accept="video/*">
    </p>
`;

export class SgMediaSelection extends HTMLElement {

    constructor() {
        super();
        addDelegatedEventListener(this, 'button[data-action=play-sample]', 'click', async (evt, button: HTMLBaseElement) => {
            const type = button.dataset.type;
            const source = button.dataset.source;
            try {
                let media: Media;
                if (type === 'video') {
                    media = await loadVideo(source);
                } else if (type === 'image') {
                    media = await loadImg(source);
                }
                if (media) {
                    this.dispatchEvent(new MediaSelected(media));
                }
            } catch (e) {
                console.error(e);
                alert('Unable to load the sample.');
            }
        });
        addDelegatedEventListener(this, 'input[name=video]', 'change', async (evt, input: HTMLInputElement) => {
            try {
                if (input.files.length) {
                    const file = input.files.item(0);
                    const media = await loadVideo(URL.createObjectURL(file));
                    this.dispatchEvent(new MediaSelected(media));
                    input.value = '';
                }
            } catch (e) {
                console.error(e);
                alert('Unable to load the sample.');
            }
        });
    }

    private get samplesElement(): HTMLDivElement {
        return this.querySelector('div.samples');
    }

    connectedCallback() {
        this.innerHTML = template;

        samples.forEach(s => {
            const button = document.createElement('button');
            button.classList.add('button');
            button.dataset.action = 'play-sample';
            button.dataset.type = s.type;
            button.dataset.source = s.source;
            button.title = `credits: ${s.credits}`;
            const i = button.appendChild(document.createElement('i'));
            s.icon.split(' ').forEach(c => i.classList.add(c))
            i.classList.add('fa-fw');
            const span = button.appendChild(document.createElement('span'));
            span.textContent = ` ${s.name}`;
            this.samplesElement.appendChild(button)
        });
    }
}

customElements.define('sg-media-selection', SgMediaSelection);
