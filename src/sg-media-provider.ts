import './sg-media-provider.scss';
import * as IncrementalDOM from 'incremental-dom';
import {MediaPlayed, SgMediaPlayer} from './sg-media-player';
import {addDelegatedEventListener, addEventListener} from '@tmorin/udom';
import {Session} from './webrtc/session';
import {ProvideOnlyHandler} from './webrtc/provide-only';
import {IoSignal} from './webrtc/signal';
import {getConfig} from './config';
import {copyTextToClipboard} from './clipboard';
import * as qrcode from 'qrcode';

const config = getConfig();

// @ts-ignore
const template = idomizer`
<tpl-if expression="data.ready">
    <h3>3. Stream it</h3>
    <div class="content">
        <tpl-if expression="data.streaming">
            <p>
                <dl class="inline">
                    <dt>roomId</dt>
                    <dd class="width">{{ data.session.roomId }}</dd>
                    <dt>peers</dt>
                    <dd class="type">{{ data.session.peers.size }}</dd>
                </dl>
            </p>
            <input type="url" name="url" readonly value="{{ data.url }}">
            <div class="two-columns">
                <button type="button" data-action="copy-url">
                    <i class="fas fa-fw fa-copy"></i>
                    copy to clipboard
                </button>
                <canvas tpl-skip></canvas>
            </div>
        <tpl-else/>
            <button type="button" data-action="share-stream">
                <i class="fas fa-share-square"></i>
                share the stream
            </button>
        </tpl-if>
    </div>
</tpl-if>
`;

export class SgMediaProvider extends HTMLElement {

    public session: Session;

    public ready = false;

    public streaming = false;

    constructor() {
        super();
        addEventListener(document, MediaPlayed.MEDIA_PLAYED, () => {
            this.ready = true;
            this.render();
        });
    }

    get url() {
        if (this.session) {
            return `${location.href.replace('/designer','/player')}?roomId=${this.session.roomId}`;
        }
        return '';
    }

    get player() {
        return this.getAttribute('player');
    }

    set player(value: string) {
        this.setAttribute('player', value);
    }

    connectedCallback() {
        this.render();
        addDelegatedEventListener(this, '[data-action=share-stream]', 'click', () => {
            this.leave();
            this.join();
        });
        addDelegatedEventListener(this, '[data-action=copy-url]', 'click', () => {
            copyTextToClipboard(this.url);
        });
        addDelegatedEventListener(this, 'input[name=url]', 'focusin', (evt, input: HTMLInputElement) => {
            input.select();
        });
    }

    private leave() {
        if (this.session) {
            this.session.leave()
        }
    }

    private join() {
        const player = document.querySelector(this.player) as SgMediaPlayer;
        if (player.stream) {
            this.streaming = true;
            const opts = {
                rtc: config.rtc,
                stream: player.stream
            };
            const signal = new IoSignal(config.io);
            this.session = Session.start(new ProvideOnlyHandler(opts, signal));
            this.session.addListener('joined', () => this.render());
            this.session.addListener('peer-joined', () => this.render());
            this.session.addListener('peer-left', () => this.render());
            this.session.addListener('peer-destroyed', () => this.render());
            this.session.addListener('peer-state-changed', (evt) => {
                console.log('peer-state-changed', evt);
                this.render();
            });
        }
    }

    private render() {
        IncrementalDOM.patch(this, template(IncrementalDOM), this);
        const canvas = this.querySelector('canvas');
        if (canvas) {
            qrcode.toCanvas(canvas, this.url, function (error) {
                if (error) {
                    console.error(error);
                }
            });
        }
    }

}

customElements.define('sg-media-provider', SgMediaProvider);
